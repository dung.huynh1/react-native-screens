function _extends() { _extends = Object.assign || function (target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i]; for (var key in source) { if (Object.prototype.hasOwnProperty.call(source, key)) { target[key] = source[key]; } } } return target; }; return _extends.apply(this, arguments); }

import React from 'react';
import { Animated, View, Image } from 'react-native';
export * from './types';
let ENABLE_SCREENS = true;
export function enableScreens(shouldEnableScreens = true) {
  ENABLE_SCREENS = shouldEnableScreens;
}
export function screensEnabled() {
  return ENABLE_SCREENS;
}
export class NativeScreen extends React.Component {
  render() {
    let {
      active,
      activityState,
      style,
      enabled = true,
      ...rest
    } = this.props;

    if (active !== undefined && activityState === undefined) {
      activityState = active !== 0 ? 2 : 0; // change taken from index.native.tsx
    }

    return /*#__PURE__*/React.createElement(View, _extends({
      style: [style, ENABLE_SCREENS && enabled && activityState !== 2 ? {
        display: 'none'
      } : null]
    }, rest));
  }

}
export const Screen = Animated.createAnimatedComponent(NativeScreen);
export const ScreenContainer = View;
export const NativeScreenContainer = View;
export const ScreenStack = View;
export const ScreenStackHeaderBackButtonImage = props => /*#__PURE__*/React.createElement(View, null, /*#__PURE__*/React.createElement(Image, _extends({
  resizeMode: "center",
  fadeDuration: 0
}, props)));
export const ScreenStackHeaderRightView = props => /*#__PURE__*/React.createElement(View, props);
export const ScreenStackHeaderLeftView = props => /*#__PURE__*/React.createElement(View, props);
export const ScreenStackHeaderCenterView = props => /*#__PURE__*/React.createElement(View, props);
export const ScreenStackHeaderConfig = View;
export const ScreenStackHeaderSubview = View;
export const shouldUseActivityState = true;
//# sourceMappingURL=index.js.map